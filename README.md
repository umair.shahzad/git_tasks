# Git_Tasks

## Hello! 
This repository has a simple Hello World! program that prints Hello World in console.

To run this program make sure you have g++ installed in your linux, mac or windows.

Download the code place it in a directory.

Go to the directory and open terminal to the directory.

Compile the program, for compiling run the following command.

**sudo g++ hello_world.cpp**

For running the program run the following command.

**./a.out**

Thank you for reading 
